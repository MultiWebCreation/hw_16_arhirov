<?php

use yii\db\Migration;

/**
 * Class m190308_103024_add_orders
 */
class m190308_103024_add_orders extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('orders', [
            'customer_name' => 'Ihor',
            'email' => 'ihor@gmail.com',
            'phone' => '+38 (093) 123-45-67',
            'feedback' => 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text.',
        ]);
        $this->insert('orders', [
            'customer_name' => 'Stepan',
            'email' => 'stepan@gmail.com',
            'phone' => '+38 (093) 123-45-68',
            'feedback' => 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text.',
        ]);
        $this->insert('orders', [
            'customer_name' => 'Helen',
            'email' => 'helen@gmail.com',
            'phone' => '+38 (093) 123-45-69',
            'feedback' => 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text.',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190308_103024_add_orders cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190308_103024_add_orders cannot be reverted.\n";

        return false;
    }
    */
}
