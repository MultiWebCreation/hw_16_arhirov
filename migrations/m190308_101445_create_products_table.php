<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%products}}`.
 */
class m190308_101445_create_products_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('products', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'alias' => $this->string(),
            'price' => $this->float(),
            'description' => $this->text(),
        ]);

        $this->createTable('orders', [
            'id' => $this->primaryKey(),
            'customer_name' => $this->string(),
            'email' => $this->string(),
            'phone' => $this->string(22),
            'feedback' => $this->text(),
        ]);

        $this->createTable('pages', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'alias' => $this->string(),
            'intro' => $this->string(),
            'description' => $this->text(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('products');
        $this->dropTable('orders');
        $this->dropTable('pages');
    }
}
