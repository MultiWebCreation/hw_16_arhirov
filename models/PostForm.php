<?php

namespace app\models;
use yii\db\ActiveRecord;


class PostForm extends ActiveRecord
{

    public static function tableName()
    {
        return 'posts';
    }

    public function attributeLabels()
    {
        return [
            'title' => 'Заголовок новости',
            'intro' => 'Превью новости',
            'description' => 'Текст новости',
        ];
    }

    public function rules()
    {
        return [
            [['title', 'intro', 'description'], 'required',],
            ['intro', 'string', 'length' => [50, 255]],
        ];
    }

}