<?php

namespace app\models;


use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * Class Comment
 * @package app\models
 *
 * @property int $itemId
 * @property string $name
 * @property string $review
 * @property Posts $post
 * @property Product $product
 */
class Comment extends ActiveRecord
{
    public static function tableName()
    {
        return 'comments';
    }

    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['name','review'], 'string'],
            [['name', 'review', 'itemId'], 'required'],
        ]);
    }

    public function getPost() {
        return $this->hasOne(Posts::class, [
            'id' => 'itemId'
        ]);
    }

    public function getProduct() {
        return $this->hasOne(Product::class, [
            'id' => 'itemId'
        ]);
    }

}