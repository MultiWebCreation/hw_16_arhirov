<?php

namespace app\models;
use yii\db\ActiveRecord;


class ProductForm extends ActiveRecord
{

    public static function tableName()
    {
        return 'products';
    }

    public function attributeLabels()
    {
        return [
            'title' => 'Название товара',
            'price' => 'Цена товара (в долларах)',
            'description' => 'Описание товара',
        ];
    }

    public function rules()
    {
        return [
            [['title', 'price', 'description'], 'required',],
            ['description', 'string', 'length' => [50, 500]],
            ['price', 'integer'],
        ];
    }

}