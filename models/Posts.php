<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "posts".
 *
 * @property int $id
 * @property string $title
 * @property string $alias
 * @property string $intro
 * @property string $description
 * @property string $name
 * @property string $review
 * @property Comment[] $comment
 */
class Posts extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'posts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['title', 'alias', 'intro'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'alias' => 'Alias',
            'intro' => 'Intro',
            'description' => 'Description',
        ];
    }

    public function getComments() {
        return $this->hasMany(Comment::class, [
            'itemId' => 'id'
        ]);
    }
}
