<?php

/**
 * @var $this yii\web\View
 * @var \app\models\Product[] $product
 * @var  \app\models\Comment[] $comments
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
$this->title = Html::encode($product->title);
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">

    <h1><?= Html::encode($this->title) ?></h1>
    <table class="table">
        <tr>
            <th>ID</th>
            <th>Название товара</th>
            <th>Цена товара</th>
            <th>Описание товара</th>
        </tr>

            <tr>
                <td><?=$product->id?></td>
                <td><?=$product->title?></td>
                <td>$<?=$product->price?></td>
                <td><?=$product->description?></td>
            </tr>
    </table>
    <br>
    <a href="/product" class="btn btn-success">Назад</a>

<h2>Отзывы</h2>
    <table class="table">
        <tr>
            <th>Имя</th>
            <th>Отзыв</th>
        </tr>
    <?php foreach ($product->comments as $value) : ?>
    <tr>
        <td><?=$value->name?></td>
        <td><?=$value->review?></td>
    </tr>
    <?php endforeach; ?>
    </table>

    <h3>Оставить отзыв</h3>
    <?php $form = ActiveForm::begin(['action' => '/product/add-comment']) ?>
    <?=$form->field($comment, 'itemId')->hiddenInput()->label('')?>
    <?=$form->field($comment, 'name')->label('Имя')?>
    <?=$form->field($comment, 'review')->textarea()->label('Отзыв')?>
    <button type="submit" class="btn btn-success">Оставить комментарий</button>
    <?php $form->end() ?>

</div>
