<?php

/**
 * @var \app\models\Product[] $product
 */

use yii\widgets\ActiveForm;
use yii\helpers\Html;

$this->title = $product->title;
$this->params['breadcrumbs'][] = $this->title;

?>
<h1>Редактировать товар "<?=$product->title?>"</h1>
<p><?=Html::a('Назад', '/product', ['class' => 'btn btn-info'])?></p>
<?php $form = ActiveForm::begin(['options' => ['id' => 'testForm']]) ?>
<div class="form-group">
    <?=$form->field($product, 'title')->input('text', ['value' => $product->title])->label('Название товара')?>
</div>
<div class="form-group">
    <?=$form->field($product, 'price')->input('text', ['value' => $product->price])->label('Цена товара')?>
</div>
<div class="form-group">
    <?=$form->field($product, 'description')->textarea(['rows' => 5, 'value' => $product->description])->label('Описание товара') ?>
</div>
<div class="form-group">
    <?=Html::submitButton('Обновить', ['class' => 'btn btn-success'])?>
</div>
<?php ActiveForm::end() ?>
