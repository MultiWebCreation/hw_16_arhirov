<?php

/**
 * @var $this yii\web\View
 * @var \app\models\Product[] $products
 */

use yii\helpers\Html;
use rmrevin\yii\fontawesome\FAS;

$this->title = 'Магазин';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>
    <p><?=Html::a('Новый товар', '/product/create', ['class' => 'btn btn-default'])?></p>

    <?php if ($products) : ?>
    <table class="table">
        <tr>
            <th>ID</th>
            <th>Название</th>
            <th>Цена</th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
        <?php foreach ($products as $product) : ?>
            <tr>
                <td><?=$product->id?></td>
                <td><?=$product->title?></td>
                <td>$<?=$product->price?></td>
                <td>
                    <?=Html::a(FAS::icon('info'),
                        [
                            '/product/view',
                            'id' => $product->id
                        ],
                        [
                            'class' => 'btn btn-info',
                            'title' => 'Подробнее'
                        ])?>
                </td>
                <td>
                    <?=Html::a(FAS::icon('edit'),
                        [
                            '/product/edit',
                            'id' => $product->id
                        ],
                        [
                            'class' => 'btn btn-success',
                            'title' => 'Редактировать'
                        ])?>
                </td>
                <td>
                    <?=Html::a(FAS::icon('trash'),
                        [
                            '/product/delete',
                            'id' => $product->id
                        ],
                        [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => 'Вы действительно хотите удалить этот товар?',
                            ],
                            'title' => 'Удалить'
                        ])?>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
    <?php else: ?>
        <p>В нашем магазине пока нет товаров...</p>
    <?php endif; ?>
</div>