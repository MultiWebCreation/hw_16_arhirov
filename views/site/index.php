<?php

/* @var $this yii\web\View */

$this->title = 'Mega Shop';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Домашнее задание 17</h1>
        <p class="lead">Создание CRUD (Новости, Магазин)</p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Товары</h2>
                <p>Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов.</p>
                <p><?=\yii\helpers\Html::a('Подробнее', '/product', ['class' => 'btn btn-info'])?></p>
            </div>
            <div class="col-lg-4">
                <h2>Заказы</h2>
                <p>Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов.</p>
                <p><?=\yii\helpers\Html::a('Подробнее', '/order', ['class' => 'btn btn-info'])?></p>
            </div>
            <div class="col-lg-4">
                <h2>Новости</h2>
                <p>Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов.</p>
                <p><?=\yii\helpers\Html::a('Подробнее', '/post', ['class' => 'btn btn-info'])?></p>
            </div>
        </div>

        <?php if ($posts) : ?>
        <div class="row">
            <div class="col-md-12">
                <h2>Последние новости</h2>
                <?php foreach ($posts as $post) : ?>
                    <p><?=\yii\helpers\Html::a($post->title, ['/post/view', 'id' => $post->id])?></p>
                <?php endforeach; ?>
            </div>
        </div>
        <?php endif; ?>

        <?php if ($products) : ?>
            <div class="row">
                <div class="col-md-12">
                    <h2>Последние товары</h2>
                    <?php foreach ($products as $product) : ?>
                        <p><?=\yii\helpers\Html::a($product->title, ['/product/view', 'id' => $product->id])?></p>
                    <?php endforeach; ?>
                </div>
            </div>
        <?php endif; ?>

    </div>
</div>
