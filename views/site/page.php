<?php

/**
 * @var $this yii\web\View
 *@var \app\models\Page[] $page
 */

use yii\helpers\Html;

$this->title = Html::encode($page->title);
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>
    <p><?=$page->description?></p>
</div>