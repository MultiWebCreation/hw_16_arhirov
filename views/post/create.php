<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

$this->title = 'Новая новость';
$this->params['breadcrumbs'][] = $this->title;

?>

<h1><?=$this->title?></h1>
<p><?=Html::a('Назад', '/post', ['class' => 'btn btn-info'])?></p>
<?php $form = ActiveForm::begin(['options' => ['id' => 'testForm']]) ?>
<div class="form-group">
<?=$form->field($model, 'title') ?>
</div>
<div class="form-group">
<?=$form->field($model, 'intro') ?>
</div>
<div class="form-group">
<?=$form->field($model, 'description')->textarea(['rows' => 5]) ?>
</div>
<div class="form-group">
<?=Html::submitButton('Создать', ['class' => 'btn btn-success'])?>
</div>
<?php ActiveForm::end() ?>
