<?php

/**
 * @var \app\models\Posts[] $post
 */

use yii\widgets\ActiveForm;
use yii\helpers\Html;

$this->title = $post->title;
$this->params['breadcrumbs'][] = $this->title;

?>
<h1>Редактировать новость "<?=$post->title?>"</h1>
<p><?=Html::a('Назад', '/post', ['class' => 'btn btn-info'])?></p>
<?php $form = ActiveForm::begin(['options' => ['id' => 'testForm']]) ?>
<div class="form-group">
    <?=$form->field($post, 'title')->input('text', ['value' => $post->title])->label('Заголовок новости')?>
</div>
<div class="form-group">
    <?=$form->field($post, 'intro')->textarea(['value' => $post->intro])->label('Превью новости')?>
</div>
<div class="form-group">
    <?=$form->field($post, 'description')->textarea(['rows' => 5, 'value' => $post->description])->label('Текст новости') ?>
</div>
<div class="form-group">
    <?=Html::submitButton('Обновить', ['class' => 'btn btn-success'])?>
</div>
<?php ActiveForm::end() ?>
