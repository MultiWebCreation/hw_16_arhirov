<?php

/**
 * @var $this yii\web\View
 * @var \app\models\Posts[] $post
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Html::encode($post->title);
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>
    <table class="table">
        <tr>
            <th>ID</th>
            <th>Заголовок</th>
            <th>Текст</th>
        </tr>
            <tr>
                <td><?=$post->id?></td>
                <td><?=$post->title?></td>
                <td><?=$post->description?></td>
            </tr>
    </table>
    <br>
    <p><?=Html::a('Назад', '/post', ['class' => 'btn btn-info'])?></p>

    <h2>Отзывы</h2>
    <table class="table">
        <tr>
            <th>Имя</th>
            <th>Отзыв</th>
        </tr>
    <?php foreach ($post->comments as $value) : ?>
    <tr>
    <td><?=$value->name?></td>
    <td><?=$value->review?></td>
    </tr>
    <?php endforeach; ?>
    </table>

    <h3>Оставить отзыв</h3>
    <?php $form = ActiveForm::begin(['action' => '/post/add-comment']) ?>
    <?=$form->field($comment, 'itemId')->hiddenInput()->label('')?>
    <?=$form->field($comment, 'name')->label('Имя')?>
    <?=$form->field($comment, 'review')->textarea()->label('Отзыв')?>
    <button type="submit" class="btn btn-success">Оставить комментарий</button>
    <?php $form->end() ?>

</div>
