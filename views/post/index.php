<?php

/**
 * @var $this yii\web\View
 * @var \app\models\Posts[] $posts
 */

use yii\helpers\Html;
use rmrevin\yii\fontawesome\FAS;

$this->title = 'Новости';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>
    <p><?=Html::a('Новая новость', '/post/create', ['class' => 'btn btn-default'])?></p>

    <?php if ($posts) : ?>
    <table class="table">
        <tr>
            <th>ID</th>
            <th>Заголовок</th>
            <th>Превью</th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
        <?php foreach ($posts as $post) : ?>
            <tr>
                <td><?=$post->id?></td>
                <td><?=$post->title?></td>
                <td><?=$post->intro?></td>
                <td>
                    <?=Html::a(FAS::icon('info'),
                        [
                            '/post/view',
                            'id' => $post->id
                        ],
                        [
                            'class' => 'btn btn-info',
                            'title' => 'Подробнее'
                        ])?>
                </td>
                <td>
                    <?=Html::a(FAS::icon('edit'),
                        [
                            '/post/edit',
                            'id' => $post->id
                        ],
                        [
                            'class' => 'btn btn-success',
                            'title' => 'Редактировать'
                        ])?>
                </td>
                <td>
                    <?=Html::a(FAS::icon('trash'),
                        [
                            '/post/delete',
                            'id' => $post->id
                        ],
                        [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => 'Вы действительно хотите удалить эту новость?',
                            ],
                            'title' => 'Удалить',
                        ])?>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
    <?php else: ?>
        <p>В данном разделе пока нет новостей...</p>
    <?php endif; ?>
</div>
