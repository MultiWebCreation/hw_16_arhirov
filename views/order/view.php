<?php

/**
 * @var $this yii\web\View
 * @var \app\models\Order[] $order
 */


foreach ($order as $value) {
    $this->title = 'Order' . $value->id;
}
$this->params['breadcrumbs'][] = 'Order ' . $this->title;
?>
<div class="site-about">
    <?php foreach ($order as $value) :?>
    <h1>Order #<?=$value->id?></h1>
    <?php endforeach; ?>
    <table class="table">
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Feedback</th>
        </tr>
        <?php foreach ($order as $value) :?>
            <tr>
                <td><?=$value->id?></td>
                <td><?=$value->customer_name?></td>
                <td><?=$value->email?></td>
                <td><?=$value->phone?></td>
                <td><?=$value->feedback?></td>
            </tr>
        <?php endforeach; ?>
    </table>
    <br>
    <a href="/order" class="btn btn-success">Back</a>
</div>
