<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Orders';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>
    <table class="table">
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th></th>
        </tr>
        <?php foreach ($orders as $order) : ?>
            <tr>
                <td><?=$order->id?></td>
                <td><?=$order->customer_name?></td>
                <td><?=$order->email?></td>
                <td><?=$order->phone?></td>
                <td><a href="/order/view/?id=<?=$order->id?>">View</a></td>
            </tr>
        <?php endforeach; ?>
    </table>
</div>
