<?php

namespace app\controllers;

use app\models\Product;
use app\models\ProductForm;
use yii\web\Controller;
use Yii;
use app\models\Comment;

class ProductController extends Controller
{
    public function actionIndex()
    {
        $products = Product::find()->all();
        return $this->render('index', compact('products'));
    }

    public function actionView($id)
    {
        $product = Product::findOne($id);
        $comment = new Comment();
        $comment->itemId = $id;
        $title = Product::find()->select('title')->where(['id' => $id])->all();
        return $this->render('view', compact('product', 'title','comment'));
    }

    public function actionCreate()
    {
        $model = new ProductForm();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Товар успешно добавлен');
                return $this->redirect('/product');
            } else {
                Yii::$app->session->setFlash('danger', 'Товар не добавлен');
            }
        }

        return $this->render('create', compact('model'));
    }

    public function actionEdit($id)
    {
        //$model = new ProductForm();
        $product = Product::findOne($id);

        if ($product->load(Yii::$app->request->post())) {
            if ($product->save()) {
                Yii::$app->session->setFlash('success', 'Товар успешно обновлен');
                return $this->redirect('/product');
            } else {
                Yii::$app->session->setFlash('danger', 'Товар не обновлен');
            }
        }

        return $this->render('edit', compact('product', 'model'));
    }

    public function actionDelete($id)
    {
        //$model = new Posts();
        $product = Product::find()->where(['id' => $id])->one();
        if ($product->delete($id)) {
            Yii::$app->session->setFlash('success', 'Товар успешно удален');
            return $this->redirect('/product');
        } else {
            Yii::$app->session->setFlash('danger', 'Товар не удален');
        }
    }

    public function actionAddComment() {
        if (Yii::$app->request->isPost) {
            $comment = new Comment();
            $comment->load(Yii::$app->request->post());
            if ($comment->save()) {
                Yii::$app->session->setFlash('success', 'Отзыв успешно добавлен');
                $this->redirect(['/product/view', 'id' => $comment->itemId]);
            } else {
                return $this->render('view', [
                    'product' => $comment->product,
                    'comment' => $comment,
                ]);
            }
        } else {
            $this->redirect('/product/index');
        }
    }

}
