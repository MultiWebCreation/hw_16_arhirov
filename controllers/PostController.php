<?php

namespace app\controllers;

use app\models\Comment;
use app\models\Posts;
use app\models\PostForm;
use Yii;

class PostController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $posts = Posts::find()->all();
        return $this->render('index', compact('posts'));
    }

    public function actionView($id)
    {
        $post = Posts::findOne($id);
        $comment = new Comment();
        $comment->itemId = $id;
        $title = Posts::find()->select('title')->where(['id' => $id]);
        return $this->render('view', compact('post', 'title', 'comment'));
    }

    public function actionCreate()
    {
        $model = new PostForm();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Новосоть успешно добавлена');
                return $this->redirect('/post');
            } else {
                Yii::$app->session->setFlash('danger', 'Новосоть не добавлена');
            }
        }

        return $this->render('create', compact('model'));
    }

    public function actionEdit($id)
    {
        //$model = new PostForm();
        $post = Posts::findOne($id);

        if ($post->load(Yii::$app->request->post())) {
            if ($post->save()) {
                Yii::$app->session->setFlash('success', 'Новость успешно обновлена');
                return $this->redirect('/post');
            } else {
                Yii::$app->session->setFlash('danger', 'Новость не обновлена');
            }
        }

        return $this->render('edit', compact('post'));
    }

    public function actionDelete($id)
    {
        //$model = new Posts();
        $post = Posts::find()->where(['id' => $id])->one();
        if ($post->delete($id)) {
            Yii::$app->session->setFlash('success', 'Новость успешно удалена');
            return $this->redirect('/post');
        } else {
            Yii::$app->session->setFlash('danger', 'Новость не удалена');
        }
    }

    public function actionAddComment() {
        if (Yii::$app->request->isPost) {
            $comment = new Comment();
            $comment->load(Yii::$app->request->post());
            if ($comment->save()) {
                Yii::$app->session->setFlash('success', 'Отзыв успешно добавлен');
                $this->redirect(['/post/view', 'id' => $comment->itemId]);
            } else {
                return $this->render('view', [
                    'post' => $comment->post,
                    'comment' => $comment,
                ]);
            }
        } else {
            $this->redirect('/post/index');
        }
    }

}
