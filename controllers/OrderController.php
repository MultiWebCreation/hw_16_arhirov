<?php

namespace app\controllers;

use app\models\Order;

class OrderController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $orders = Order::find()->all();
        return $this->render('index', [
            'orders' => $orders,
        ]);
    }

    public function actionView($id)
    {
        $order = Order::find()->where(['id' => $id])->all();
        //$title = Order::find()->select('title')->where(['id' => $id])->all();
        return $this->render('view', [
            'order' => $order,
        ]);
    }

}
